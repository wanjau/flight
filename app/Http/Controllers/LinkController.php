<?php

namespace App\Http\Controllers;

use App\Models\Links;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\URL;
use Illuminate\Support\Str;

class LinkController extends Controller
{
    /**
     * returning errors in api like when a resource is not found
     * securing/authentication/authorization
     * returning validation errors
     * reading values returned from an api
     * updating a resource
     * authentication
     * validation
     * deploying laravel apis
     * rate limiting in api
     * versioning v1 using prefix or storing user with the cersion they are using
     * graphql
     * @return void
     */


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //view all links
        return Links::all();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //create a short link
        $validateData = $request->validate([
            'original_link' => 'required|url'
        ]);

        $randomtoken = Str::random(8);

        $original_link = $request['original_link'];

        $links = new Links();

        $links->original_link = $original_link;
        $links->short_link = URL::to('/') . '/' .$randomtoken;

        //return $links;

        $links->save();

        return $links;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return Links::find($id);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //create a short link
        // $validateData = $request->validate([
        //     'original_link' => 'required|url'
        // ]);

        $randomtoken = Str::random(8);

        $original_link = $request->original_link;

        $links = Links::find($id);

        $links->original_link = $original_link;

        $links->short_link = URL::to('/') . '/' .$randomtoken;

        $links->save();

        return $links;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $link =  Links::destroy($id);

        return $link;
    }
}
