<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::namespace('App\Http\Controllers')->group(function () {
    // Route::get('/', 'LinksController@index')->name('index');

    // Route::post('shorten', 'LinksController@shorten')->name('shorten');

    // Route::get('/{link}', 'LinksController@retrieveLink')->name('retrieve.link');

    Route::get('/', 'MoviesController@index')->name('movies.index');
    Route::get('/movies/reset', 'MoviesController@reset')->name('movies.reset');

    Route::post('/movies', 'MoviesController@flightTime')->name('movies.flightTime');

    Route::get('/movies/select/{id?}/{duration?}', 'MoviesController@selectedMovies')->name('movies.selected');
    
});   