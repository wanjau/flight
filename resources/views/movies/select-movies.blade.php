<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>Movies</title>

  <link rel="stylesheet" href="{{ asset('css/app.css') }}">

  <style>
    body{
      background-color: white;
    }

    .top-bar{
      background-color: purple;
      color: #fff;
    }
  </style>

</head>
<body>

<section class="top-bar py-5">
    <div class="top-bar text-center">
      <h2>Movies</h2>
    </div>
</section>


<section class="py-5">

  <div class="container">
    <div class="row">
      <div class="col-md-10 mx-auto">

        <div class="panel panel-primary">
          <div class="panel-heading pb-3">
              @if(Session::has('error'))
                <div class="alert alert-danger">
                  {{ Session::get('error') }}
                </div>
              @endif

              @if(Session::has('success'))
                <div class="alert alert-success">
                  {{ Session::get('success') }}
                </div>
              @endif

            <h3>Select Two Movies to Watch</h3>
            <a href="{{ route('movies.reset') }}" class="btn btn-success pull-left"> Reset</a>
          </div>

          <div class="panel-body">
            <table class="table table-stripped">
              <thead>
                <tr>
                  <th>Id</th>
                  <th>Name</th>
                  <th>Duration</th>
                  <th>Action</th>
                </tr>
              </thead>

              <tbody>
                  @if(Session::has('ids'))
            
                    <tr>
                      <td>1</td>
                      <td>Movie 1</td>
                      <td>30 min</td>
                      <td>
                        <a href="{{ route('movies.selected', ['id'=>1, 'duration'=>30]) }}" data-id="1" class="btn {{ in_array(1, Session::get('ids')) ? 'btn-success' : 'btn-primary' }}">{{ in_array(1, Session::get('ids')) ? 'Selected' : 'Select' }}</a>
                      </td>
                    </tr>

                    <tr>
                      <td>2</td>
                      <td>Movie 2</td>
                      <td>20 min</td>
                      <td>
                        <a href="{{ route('movies.selected', ['id'=>2, 'duration'=>20]) }}" data-id="2" class="btn {{ in_array(2, Session::get('ids')) ? 'btn-success' : 'btn-primary' }}">{{ in_array(2, Session::get('ids')) ? 'Selected' : 'Select' }}</a>
                      </td>
                    </tr>

                    <tr>
                      <td>3</td>
                      <td>Movie 3</td>
                      <td>40 min</td>
                      <td>
                        <a href="{{ route('movies.selected', ['id'=>3, 'duration'=>40]) }}" data-id="3" class="btn {{ in_array(3, Session::get('ids')) ? 'btn-success' : 'btn-primary' }}">{{ in_array(3, Session::get('ids')) ? 'Selected' : 'Select' }}</a>
                      </td>
                    </tr>

                    <tr>
                      <td>4</td>
                      <td>Movie 4</td>
                      <td>30 min</td>
                      <td>
                        <a href="{{ route('movies.selected', ['id'=>4, 'duration'=>30]) }}" data-id="4" class="btn {{ in_array(4, Session::get('ids')) ? 'btn-success' : 'btn-primary' }}">{{ in_array(4, Session::get('ids')) ? 'Selected' : 'Select' }}</a>
                      </td>
                    </tr>
                   
                  @else
                    <tr>
                      <td>1</td>
                      <td>Movie 1</td>
                      <td>30 min</td>
                      <td>
                        <a href="{{ route('movies.selected', ['id'=>1, 'duration'=>30]) }}" data-id="1" class="btn btn-primary">Select</a>
                      </td>
                    </tr>

                    <tr>
                      <td>2</td>
                      <td>Movie 2</td>
                      <td>20 min</td>
                      <td>
                        <a href="{{ route('movies.selected', ['id'=>2, 'duration'=>20]) }}" data-id="2" class="btn btn-primary">Select</a>
                      </td>
                    </tr>

                    <tr>
                      <td>3</td>
                      <td>Movie 3</td>
                      <td>40 min</td>
                      <td>
                        <a href="{{ route('movies.selected', ['id'=>3, 'duration'=>40]) }}" data-id="3" class="btn btn-primary">Select</a>
                      </td>
                    </tr>

                    <tr>
                      <td>4</td>
                      <td>Movie 4</td>
                      <td>30 min</td>
                      <td>
                        <a href="{{ route('movies.selected', ['id'=>4, 'duration'=>30]) }}" data-id="4" class="btn btn-primary">Select</a>
                      </td>
                    </tr>
                  @endif  
              </tbody>

            </table>
          </div>
        </div>


      </div>
    </div>
  </div> 

</section>

  <script src="{{ asset('js/app.js') }}"></script>
</body>
</html>