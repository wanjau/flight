<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

class MoviesController extends Controller
{
    public function index(){

        return view('movies.index');
    }

    public function flightTime(Request $request){
        $request->validate([
            'flight_time' => 'required|numeric'
        ]);

        //create session
        Session::put('flight_time', $request->flight_time);

        return redirect()->route('movies.selected');
    }

    public function reset(){
        Session::flush();

        return redirect()->route('movies.index');
    }

    public function selectedMovies($id = null, $duration = null){

         Session::forget('success');
         Session::forget('error');

        if($id != null && $duration != null){

            if(Session::has('selected') && Session::get('selected') == 1) {

                //timeremaining
                $timeremaining = Session::get('flight_time') - $duration;

                if($timeremaining < 0){
                    
                    Session::flash('error', '1st and 2nd movie duration is more than the flight length');
                     return view('movies.select-movies');
                     
                }else{
                    
                    //you cannot repeate a movie
                    if(Session::get('ids')[0] != $id){
                        Session::put('selected', 2);

                        $selectedmovie = Session::get('ids');
                        array_push($selectedmovie, $id);

                        Session::put('ids', $selectedmovie);

                        if($timeremaining == $duration){
                            Session::flash('success', 'Perfect! The two movies duration equal the flight length');
                        }else{
                            Session::flash('success', 'Perfect! The two movies duration are within your flight length');
                        }   
                    }else{
                        Session::flash('error', 'You cannot repeat a movie');
                    }

                }

            }elseif(Session::has('selected') && Session::get('selected') == 2){
                Session::forget('success');
                Session::flash('error', 'You cannot select more than two movies');

            }else{
                     
                //timeremaining
                $timeremaining = Session::get('flight_time') - $duration;

                if($timeremaining < 0){
                    Session::flash('error', 'Movie duration is more than the flight length');
                     return view('movies.select-movies');
                     
                }else{
                    Session::flash('success', 'Very nice! Select a second movie');
                    Session::put('selected', 1);

                    $selectedmovie = [$id];

                    Session::put('flight_time', $timeremaining);

                    Session::put('ids', $selectedmovie);
                }

                
            }

        }

        return view('movies.select-movies');
    }
}
