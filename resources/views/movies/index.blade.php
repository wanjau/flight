<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>Movies</title>

  <link rel="stylesheet" href="{{ asset('css/app.css') }}">

  <style>
    body{
      background-color: white;
    }

    .top-bar{
      background-color: purple;
      color: #fff;
    }
  </style>

</head>
<body>

<section class="top-bar py-5">
    <div class="top-bar text-center">
      <h2>Movies</h2>
    </div>
</section>


<section class="py-5">

  <div class="container">
    <div class="row">
      <div class="col-md-10 mx-auto">

        <div class="panel panel-primary">
          <div class="panel-heading"></div>

          <div class="panel-body">
            <form action="{{ route('movies.flightTime') }}" method="POST">
              @csrf

              <div class="form-group">
                <label for="shorten">Length of Flight</label>
                <input type="number" class="form-control" name="flight_time" required placeholder="Enter length of flight in minutes">

                 @error('flight_time')
                  <p class="text-danger">{{ $message }}</p>
                 @enderror 
              </div>

              <div class="form-group">
                <button type="submit" class="btn btn-primary">Save</button>  
              </div>
            
            </form>
          </div>
        </div>


      </div>
    </div>
  </div> 

</section>

  <script src="{{ asset('js/app.js') }}"></script>
</body>
</html>